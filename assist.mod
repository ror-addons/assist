<?xml version="1.0" encoding="UTF-8"?>

<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <UiMod name="Assist" version="1.1" date="08/20/2010">

      <Author name="gutgut" email="" />

      <Description text="Easy Assist" />
      <VersionSettings gameVersion="1.5.5" windowsVersion="1.0" savedVariablesVersion="1.1" /> 
      <Dependencies>
  		<Dependency name="EA_TacticsWindow" />
        <Dependency name="LibSlash" />
        <Dependency name="EA_PlayerMenu" />
      </Dependencies>

      <Files>
  		<File name="Assist.xml" />
      </Files>

      <OnInitialize>
        <CallFunction name="Assist.OnInitialize" />
      </OnInitialize>
      
      <OnUpdate>
        <CallFunction name="Assist.OnUpdate" />
      </OnUpdate>

      <OnShutdown>
        <CallFunction name="Assist.OnShutdown" />
      </OnShutdown>
		
      <SavedVariables>
		<SavedVariable name="Assist.Settings" />
      </SavedVariables>

    </UiMod>

</ModuleFile>