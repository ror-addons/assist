------------------------------------------------
--   Assist
-- 		by gutgut
------------------------------------------------

if not Assist then Assist = {} end

Assist.isDisabled = false
Assist.Buttons={}
Assist.Buttons[1]={descr=L"Set Assist Target 1",funcR="Assist.SetCurrentFT",funcL="Assist.leer",btext=L"A",hbnr=61}
Assist.Buttons[2]={descr=L"Set Assist Target 1",funcR="Assist.SetCurrentFT",funcL="Assist.leer",btext=L"A",hbnr=62}
Assist.Buttons[3]={descr=L"Set Assist Target 1",funcR="Assist.SetCurrentFT",funcL="Assist.leer",btext=L"A",hbnr=63}
Assist.Buttons[4]={descr=L"Set Hostile Target 1",funcR="Assist.SetCurrentHT",funcL="Assist.ActH",btext=L"M",hbnr=64}
Assist.Buttons[5]={descr=L"Set Hostile Target 2",funcR="Assist.SetCurrentHT",funcL="Assist.ActH",btext=L"M",hbnr=65}
Assist.Buttons[6]={descr=L"Set Hostile Target 3",funcR="Assist.SetCurrentHT",funcL="Assist.ActH",btext=L"M",hbnr=66}
Assist.Buttons[7]={descr=L"Set Friendly Target 1",funcR="Assist.SetCurrentFT",funcL="Assist.ActF",btext=L"M",hbnr=67}
Assist.Buttons[8]={descr=L"Set Friendly Target 2",funcR="Assist.SetCurrentFT",funcL="Assist.ActF",btext=L"M",hbnr=68}
Assist.Buttons[9]={descr=L"Set Friendly Target 3",funcR="Assist.SetCurrentFT",funcL="Assist.ActF",btext=L"M",hbnr=69}




Assist.Settings = {}

Assist.Oldfunc = {}

Assist.Settings.t1=L"-"
Assist.Settings.t2=L"-"
Assist.Settings.t3=L"-"
Assist.Settings.t4=L"-"
Assist.Settings.t5=L"-"
Assist.Settings.t6=L"-"

Assist.Settings.auto = {[4]=true,[5]=true,[6]=true}
Assist.hdam = {peak=0,id=L""}
Assist.hheal = {peak=0,id=L""}
Assist.hdbl = {peak=0,id=L""}

--d(L"load")

function Assist.Setat1()
	Assist.Settings.t1=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.Setat2()
	Assist.Settings.t2=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.Setat3()
	Assist.Settings.t3=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.Setht1()
	Assist.Settings.t4=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.Setht2()
	Assist.Settings.t5=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.Setht3()
	Assist.Settings.t6=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end


function Assist.Setft1()
	Assist.Settings.t7=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.Setft2()
	Assist.Settings.t8=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.Setft3()
	Assist.Settings.t9=PlayerMenuWindow.curPlayer.name
	Assist.updTargets()
end

function Assist.ActF()
 local Selection = tonumber(WindowGetId( SystemData.ActiveWindow.name ))
 d(Selection)
 SendChatText( L"/target "..Assist.Settings["t"..Selection], L"" )
end

function Assist.ActH()
 local Selection = tonumber(WindowGetId( SystemData.ActiveWindow.name ))
 d(Selection)
 SendChatText( L"/target "..Assist.Settings["t"..Selection], L"" )
end



function Assist.SetCurrentHT()

	local Selection = tonumber(WindowGetId( SystemData.MouseOverWindow.name ))
	local pname = L""..TargetInfo:UnitName(TargetInfo.HOSTILE_TARGET)
	
	if pname > L"" then 
		Assist.Settings["t"..Selection]=WStringsRemoveGrammar(pname)
		ButtonSetText(SystemData.MouseOverWindow.name, L"M") 
		Assist.Settings.auto[Selection]=false
	else
		Assist.Settings["t"..Selection]=L"-"
		ButtonSetText(SystemData.MouseOverWindow.name, L"T")
		Assist.Settings.auto[Selection]=true	 
	end
	Assist.updTargets()
end



function Assist.SetCurrentFT()
	local Selection = tonumber(WindowGetId( SystemData.ActiveWindow.name ))
	local pname = L""..TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET)
	if pname > L"" then 
		--pname=wstring.gsub( pname, L"(^.)", L"" )
		Assist.Settings["t"..Selection]=WStringsRemoveGrammar(pname)
	else
		Assist.Settings["t"..Selection]=L"-" 
	end
	Assist.updTargets()
end

function Assist.leer()
end



function Assist.updTargets()
	
	SetMacroData( L"Assist "..Assist.Settings.t1, L"/assist "..Assist.Settings.t1, 146, 46 )
	SetMacroData( L"Assist "..Assist.Settings.t2, L"/assist "..Assist.Settings.t2, 146, 47 )
	SetMacroData( L"Assist "..Assist.Settings.t3, L"/assist "..Assist.Settings.t3, 146, 48 )
	
	WindowSetGameActionData( "AssistWindow1BTN", 4, 46, L"" )
	WindowSetGameActionData( "AssistWindow2BTN", 4, 47, L"" )
    WindowSetGameActionData( "AssistWindow3BTN", 4, 48, L"" )
    
    Assist.AssistWindowOnShown()
    
end


function Assist.RegisterEvents()
        --d(L"RegEvents")
         --RegisterEventHandler( SystemData.Events.M_BUTTON_DOWN_PROCESSED, "Assist.MenuOpen" )
         --WindowRegisterEventHandler( "AssistWindow", SystemData.Events.M_BUTTON_UP_PROCESSED, "Assist.MenuClose" )
         
         Assist.Oldfunc=PlayerMenuWindow.ShowMenu
		PlayerMenuWindow.ShowMenu=Assist.PlayerMenuWindowShowMenu

end

function Assist.UnregisterEvents()
         --UnregisterEventHandler( SystemData.Events.M_BUTTON_DOWN_PROCESSED, "Assist.MenuOpen" )
         --WindowUnregisterEventHandler( "AssistWindowOverlay", SystemData.Events.M_BUTTON_UP_PROCESSED, "Assist.MenuClose" )
         
         PlayerMenuWindow.ShowMenu=Assist.Oldfunc
end

function Assist.InitializeDefaults()
         Assist.Defaults = {
                              ["x"]                = "0",
                              ["x1"]                  = "1",
                            }
end

function Assist.SetSetting(key, value)
         Assist.Settings[key] = value
end

function Assist.GetSetting(key)
         if Assist.Settings[key] ~= nil then
             return Assist.Settings[key]
         else
             if Assist.Defaults[key] ~= nil then
                 Assist.Settings[key] = Assist.Defaults[key]
                 return Assist.Settings[key]
             else
                 return nil
             end
         end
end

function Assist.OnInitialize()

         Assist.SetupButtons()
         
         if not Assist.Settings then Assist.Settings = {} end
         if not Assist.Defaults then Assist.Defaults = {} end


         
         Assist.InitializeDefaults()
         Assist.RegisterEvents()
         
         Assist.updTargets()
         LibSlash.RegisterSlashCmd("Assist", function(input) Assist.SlashHandler(input) end)

end

function Assist.SetupButtons()

	    for i, item in ipairs(Assist.Buttons) do
			if not item.set then
				local window = "AssistWindow"..tostring(i)
				local button = window.."BTN"
	 
			    CreateWindowFromTemplate(window, "AssistWindow", "Root")
			    WindowSetId(button, i)
			    LayoutEditor.RegisterWindow(window,L"AssistButton"..towstring(i),item.descr,true,true,true,nil )
			    WindowRegisterCoreEventHandler(button, "OnRButtonUp", item.funcR)
			    WindowRegisterCoreEventHandler(button, "OnLButtonUp", item.funcL)
			    WindowSetGameActionTrigger (button, GetActionIdFromName ("ACTION_BAR_"..tostring(item.hbnr)))
			    if Assist.Settings.auto[i] then
			    	ButtonSetText(button, L"T")	
			    else
			    	ButtonSetText(button, item.btext) 
			    end
				WindowSetShowing( window, true )
				item.set=true
			end
		end

end

function Assist.OnShutdown()
         Assist.UnregisterEvents()
         Assist.DestroyWindows()
end




function Assist.SlashHandler(args)
 local opt, val = args:match("([a-z0-9]+)[ ]?(.*)")

         if opt == "x1" then Assist.SetRVR(val)
         elseif opt == "x2" then Assist.SetQueue(val)
         else
             EA_ChatWindow.Print( towstring( "Available commands for /Assist: [none]" ) )
         end
end



function Assist.AssistWindowOnShown()
 	for i, item in ipairs(Assist.Buttons) do
 		 LabelSetText("AssistWindow"..i.."Text", Assist.Settings["t"..i])
	end
end

function Assist.addButton(adescr,afuncR,afuncL,abtext,ahbnr,content)
	local num = #Assist.Buttons+1
	Assist.Settings["t"..num]=content
	Assist.Buttons[num]={descr=adescr,funcR=afuncR,funcL=afuncL,btext=abtext,hbnr=ahbnr}
	Assist.SetupButtons()		
end


function Assist.CheckSCPlayers()
	if GameData.Player.isInScenario or GameData.Player.isInSiege then
	    BroadcastEvent(SystemData.Events.SCENARIO_START_UPDATING_PLAYERS_STATS) 
	   	local sl = GameData.GetScenarioPlayers()
	   	BroadcastEvent(SystemData.Events.SCENARIO_STOP_UPDATING_PLAYERS_STATS )
		if not sl then return end
		
		Assist.hdam.peak = 0
		Assist.hheal.peak = 0
		Assist.hdbl.peak = 0
		
		for i, pl in pairs(sl) do
			if GameData.Player.realm ~= pl.realm then
  
			    
				if Assist.hdam.peak < pl.damagedealt then Assist.hdam.peak = pl.damagedealt; Assist.hdam.id = WStringsRemoveGrammar(pl.name); end
				if Assist.hheal.peak < pl.healingdealt then Assist.hheal.peak = pl.healingdealt; Assist.hheal.id = WStringsRemoveGrammar(pl.name); end
				if Assist.hdbl.peak < pl.deathblows then Assist.hdbl.peak = pl.deathblows; Assist.hdbl.id = WStringsRemoveGrammar(pl.name); end
			
		    end
		end
	    
		if Assist.Settings.auto[4] and Assist.Settings.t4 ~= Assist.hdam.id then Assist.Settings.t4 = Assist.hdam.id; Assist.updTargets() end
		if Assist.Settings.auto[5] and Assist.Settings.t5 ~= Assist.hheal.id then Assist.Settings.t5 = Assist.hheal.id; Assist.updTargets() end
		if Assist.Settings.auto[6] and Assist.Settings.t6 ~= Assist.hdbl.id then Assist.Settings.t6 = Assist.hdbl.id; Assist.updTargets() end

	end

end



------------------------------------------------



Assist.Metronome = 1.5
Assist.timeLeft = Assist.Metronome

function Assist.OnUpdate(elapsed)
	 Assist.timeLeft = Assist.timeLeft - elapsed
	 if Assist.timeLeft > 0 then return end

	Assist.CheckSCPlayers()
	Assist.timeLeft = Assist.Metronome

end





--------------------------------------------------------------------------------------------------------

function Assist.PlayerMenuWindowShowMenu( playerName, playerObjNum, customItems ,...) 
    

	if type(customItems) ~= "table" then customItems = {} end
	table.insert(customItems,PlayerMenuWindow.NewCustomItem( L"Set as Assist 1", Assist.Setat1, false))      
    table.insert(customItems,PlayerMenuWindow.NewCustomItem( L"Set as Assist 2", Assist.Setat2, false))       
    table.insert(customItems,PlayerMenuWindow.NewCustomItem( L"Set as Assist 3", Assist.Setat3, false))       
    
	table.insert(customItems,PlayerMenuWindow.NewCustomItem( L"Set as Friendly 1", Assist.Setft1, false))      
    table.insert(customItems,PlayerMenuWindow.NewCustomItem( L"Set as Friendly 2", Assist.Setft2, false))       
    table.insert(customItems,PlayerMenuWindow.NewCustomItem( L"Set as Friendly 3", Assist.Setft3, false))      
	


    Assist.Oldfunc( playerName, playerObjNum, customItems ,...)
end